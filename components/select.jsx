import React, { useState,useEffect,useRef } from 'react';

const Index = (props) => {
    const [select, setSelect] = useState("")
    const [active, setActive] = useState(false)
    const [classNameAdd, setclassNameAdd] = useState("")
    const optionsRef = useRef(null);
    const {
        placeholder,
        className,
        classNameInput,
        onChange,
        options,
        defaultValue,
        disabled,
        value,

    } = {
        placeholder : "",
        className : "",
        classNameInput : "",
        onChange : (e)=>console.log(e),
        options : {},
        defaultValue:null,
        disabled : false,
        value:null,

        ...props
    }
    useEffect(() => {
        if(active){
            const element = optionsRef.current
            const parent = element.parentElement
            const topParent = parent.offsetTop
            const heightParent = parent.offsetHeight
            const totalHeight = window.innerHeight
            const totalScroll = window.scrollY

            const spaceDown = totalHeight - ( topParent + heightParent  - totalScroll)
            const spaceTop = topParent - totalScroll
            const space = Math.max(spaceDown,spaceTop)
            if(spaceDown < 300){
                if(spaceTop > spaceDown){
                    setclassNameAdd("invert")
                }else{
                    setclassNameAdd("")
                }
                optionsRef.current.style.maxHeight= `${space}px`
            }
        }
    }, [active]);
    return(
        <div className={`select ${className} ${classNameAdd} ${active ? "active" : ""}`} onClick={()=>{if(!disabled){setActive(!active)}}} onMouseLeave={()=>{setActive(false)}}>
            <input 
                className={`input ${classNameInput}`}
                type="text"
                placeholder={placeholder}
                onChange={()=>{}}
                value={(options[value] || {}).text || options[value] || options[select] || options[defaultValue] || ""}
                spellCheck="false"
                disabled={disabled}
                autoComplete="off"
            />
            <svg viewBox="0 0 25 17" fill="none" xmlns="http://www.w3.org/2000/svg" className="icon">
                <path d="M13.3059 15.9002C13.213 16.0265 13.0916 16.1292 12.9517 16.2C12.8118 16.2707 12.6572 16.3076 12.5004 16.3076C12.3436 16.3076 12.189 16.2707 12.0491 16.2C11.9091 16.1292 11.7878 16.0265 11.6949 15.9002L1.17089 1.59221C1.06144 1.44322 0.995443 1.2668 0.980221 1.08256C0.964998 0.898315 1.00115 0.713459 1.08465 0.548522C1.16816 0.383585 1.29575 0.245024 1.45325 0.148232C1.61076 0.0514393 1.79202 0.000202179 1.97689 0.000213623L23.0229 0.000213623C23.2078 0.000202179 23.389 0.0514393 23.5465 0.148232C23.704 0.245024 23.8316 0.383585 23.9151 0.548522C23.9986 0.713459 24.0348 0.898315 24.0196 1.08256C24.0043 1.2668 23.9383 1.44322 23.8289 1.59221L13.3059 15.9002Z" fill="var(--warm-grey-three)"/>
            </svg>
            <div className="options" ref={optionsRef} >
                {Object.keys(options).map((e,i)=>{
                    return (
                        <div className="option" key={i} onClick={()=>{setSelect(e);onChange(e)}}>
                            {
                                options[e].option == null ?
                                options[e]
                                :
                                options[e].option
                            }
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
export default Index