const Index = (props) => {
    const { onClick, text, className,disabled } = {
        onClick: (e) => console.log(e),
        text: "",
        className: "",
        disabled:false,

        ...props,
    };
    return (
        <button className={`${className} ${disabled?"disabled":""}`} onClick={!disabled?onClick:()=>{}} disabled={disabled}>
            {text}
        </button>
    );
};
export default Index;
