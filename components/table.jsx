const Index = (props) => {
    const { ths, rows, pages, className } = {
        ths: [],
        rows: [],
        pages: {},
        className: "",

        ...props,
    };
    return (
        <div className="contentTable">
            <table className={`table ${className}`}>
                <thead>
                    <tr>
                        {ths.map((e, i) => {
                            return <th key={i}>{e.html}</th>;
                        })}
                    </tr>
                </thead>
                <tbody>
                    {rows.map((row, i) => {
                        return (
                            <tr key={i}>
                                {ths.map((ele, j) => {
                                    return (
                                        <td key={i + "-" + j}>
                                            {ele.print(row[ele.id])}
                                        </td>
                                    );
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
};
export default Index;
