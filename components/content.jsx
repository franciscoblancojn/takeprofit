import NavLeft from "@/components/navLeft";

const Index = (props) => {
    const { children, menu } = {
        children: "",
        menu: false,

        ...props,
    };
    return (
        <>
            {menu && <NavLeft />}
            {children}
        </>
    );
};
export default Index;
