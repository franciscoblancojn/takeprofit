import React, { useRef } from 'react';

const Index = (props) => {
    const {
        type,
        placeholder,
        className,
        onChange,
        defaultValue,
        disabled,
        value,
        moreProps,
    } = {
        type : "text",
        placeholder : "",
        className : "",
        onChange : (e)=>console.log(e),
        defaultValue : "",
        disabled : false,
        value : null,
        moreProps : {},

        ...props
    }
    const add = {}
    if(value !==  null){
        add.value = value
    }else{
        add.defaultValue = defaultValue
    }
    return(
        <input 
            className={`input ${className}`}
            type={type}
            placeholder={placeholder}
            onChange={onChange}
            disabled={disabled}
            {...add}
            {...moreProps}
        />
    )
}
export default Index