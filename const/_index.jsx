import c_banks from "@/const/banks";
export const banks = c_banks;

import c_typeCuentas from "@/const/typeCuentas";
export const typeCuentas = c_typeCuentas;

import c_paices from "@/const/paices";
export const paices = c_paices;

import c_provincias from "@/const/provincias";
export const provincias = c_provincias;

import c_ciudades from "@/const/ciudades";
export const ciudades = c_ciudades;
