import jsHttpCookie from "cookie";
import verify from "@/api/verify";

export default async function getServerSideProps({ req, res }) {
    const cookies = req.headers.cookie;
    var token = null;
    if (typeof cookies === "string") {
        const cookiesJSON = jsHttpCookie.parse(cookies);
        token = cookiesJSON.token;
    }
    if (process.env.MODE === "DEV") {
        console.log("%c [COOKIES]", "color:aqua;", { cookies });
        console.log("%c [TOKEN]", "color:aqua;", { token });
    }
    if (!token) {
        return {
            redirect: {
                permanent: false,
                destination: "/login",
            },
            props: {},
        };
    }else{
        const result = await verify(token)
        if (process.env.MODE === "DEV") {
            console.log("%c [VERIFY]", "color:aqua;", result );
        }
        if(result.type !== "ok"){
            return {
                redirect: {
                    permanent: false,
                    destination: "/login",
                },
                props: {},
            };
        }
    }
    return {
        props: { token }, // Will be passed to the page component as props
    };
}
