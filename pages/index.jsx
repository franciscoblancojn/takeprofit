import React, { useState, useEffect } from "react";
import Link from "next/link";
import jwt_decode from "jwt-decode";

import getToken from "@/servers/getToken";

import { money, log } from "@/functions/_index";

import Content from "@/components/content";
import Space from "@/components/space";

const Index = ({token}) => {
    const user = jwt_decode(token);
    log("user", user, "aqua");
    // const [user, setUser] = useState({
    //     name: "Mario",
    //     email: "test",
    //     consolidado: null,
    //     capital: null,
    //     utilidades_liquidadas: null,
    //     proximo_retiro_disponible: {
    //         money: 500,
    //         periodo: 2,
    //     },
    //     proximo_retiro_disponible: null,
    //     acount: "xxxxxxxxx48",
    //     banco: "Bancolombia",
    // });
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Resumen</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 col-md-6">
                    <p className="mt-0 mb-0 color-warm-grey">
                        Hola{" "}
                        <strong>
                            {user.name == null || user.name == ""
                                ? user.email
                                : user.name}
                        </strong>
                        , este es el resumen de tu cuenta.
                    </p>
                </div>
                <div className="col-12 col-md-6">
                    <Link href="/retiros/historial">
                        <a className="">Ver Historial de Retiros</a>
                    </Link>
                </div>
                <div
                    className="col-12 mb-69"
                    style={{ borderBottom: "1px solid var(--pinkish-grey)" }}
                >
                    <Space space="50" />
                </div>
                <div className="col-12 flex">
                    <div className="card mr-27 mb-28">
                        {user.consolidado == null ? (
                            <div className="align-center">
                                <div
                                    className="color-black-Two font-size-25"
                                    style={{ opacity: 0.5 }}
                                >
                                    Consolidado, Nada Aun
                                </div>
                                <img
                                    src="/img/nadaAun.svg"
                                    alt="Nada Aun"
                                    width="61.41"
                                />
                            </div>
                        ) : (
                            <>
                                <h6>Consolidado</h6>
                                <Space space="28" />
                                <span className="money">
                                    {money(user.consolidado)}
                                </span>
                            </>
                        )}
                    </div>
                    <div className="card mr-27 mb-28">
                        {user.capital == null ? (
                            <div className="align-center">
                                <div
                                    className="color-black-Two font-size-25"
                                    style={{ opacity: 0.5 }}
                                >
                                    Capital, Nada Aun
                                </div>
                                <img
                                    src="/img/nadaAun.svg"
                                    alt="Nada Aun"
                                    width="61.41"
                                />
                            </div>
                        ) : (
                            <>
                                <h6>Capital</h6>
                                <Space space="28" />
                                <span className="money">
                                    {money(user.capital)}
                                </span>
                            </>
                        )}
                    </div>
                    <div className="card mb-28">
                        {user.utilidades_liquidadas == null ? (
                            <div className="align-center">
                                <div
                                    className="color-black-Two font-size-25"
                                    style={{ opacity: 0.5 }}
                                >
                                    Utilidades Liquidadas, Nada Aun
                                </div>
                                <img
                                    src="/img/nadaAun.svg"
                                    alt="Nada Aun"
                                    width="61.41"
                                />
                            </div>
                        ) : (
                            <>
                                <h6>Utilidades Liquidadas</h6>
                                <Space space="28" />
                                <span className="money">
                                    {money(user.utilidades_liquidadas)}
                                </span>
                            </>
                        )}
                    </div>
                </div>
                <div className="col-12">
                    <div className="card">
                        {user.proximo_retiro_disponible == null ? (
                            <div className="align-center">
                                <div
                                    className="color-black-Two font-size-25"
                                    style={{ opacity: 0.5 }}
                                >
                                    Utilidades Liquidadas, Nada Aun
                                </div>
                                <img
                                    src="/img/nadaAun.svg"
                                    alt="Nada Aun"
                                    width="61.41"
                                />
                            </div>
                        ) : (
                            <>
                                <h6>Próximo Retiro Disponible</h6>
                                <Space space="28" />
                                <div
                                    className="money flex flex-align-center"
                                    style={{ opacity: 0.5 }}
                                >
                                    <span>
                                        {money(
                                            user.proximo_retiro_disponible.money
                                        )}{" "}
                                        / Periodo{" "}
                                        {user.proximo_retiro_disponible.periodo}{" "}
                                        /
                                    </span>
                                    <img
                                        src={`/banks/${user.banco}.png`}
                                        alt={user.banco}
                                        className="mr-10 ml-10"
                                    />
                                    <span className="flex font-size-20">
                                        <strong className="w-100 color-black">
                                            {user.banco}
                                        </strong>
                                        {user.acount}
                                    </span>
                                </div>
                            </>
                        )}
                    </div>
                </div>
                <div className="col-12 pb-25"></div>
            </div>
        </Content>
    );
};
export const getServerSideProps = getToken;
export default Index;
