import React, { useState, useEffect } from "react";
import jwt_decode from "jwt-decode";

import getToken from "@/servers/getToken";
import Cookies from "js-cookie";

import { accounts } from "@/api/api";
import { log, toStringTwoDigitDate } from "@/functions/_index";
import { paices, provincias, ciudades } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Input from "@/components/input";
import Select from "@/components/select";
import Space from "@/components/space";
import Button from "@/components/button";

const ItemsBreadcrumbs = [
    {
        href: "/mi-cuenta",
        text: "Mi Cuenta",
    },
];
const Index = ({ token, account }) => {
    log("ACCOUNT", account, "aqua");
    const [respond, setRespond] = useState("");
    const [edit, setEdit] = useState(false);
    const [changeData, setChangeData] = useState(false);
    const [data, setData] = useState(account);
    const [dataCancel, setDataCancel] = useState(null);
    const updateData = (id) => (e) => {
        if (id === "pais") {
            setData({ ...data, [id]: e, provincia: "", ciudad: "" });
            setChangeData(true);
            return;
        }
        if (data.pais === "COL") {
            if (id === "provincia") {
                setData({ ...data, [id]: e, ciudad: "" });
                setChangeData(true);
                return;
            }
            if (id === "ciudad") {
                setData({ ...data, [id]: e });
                setChangeData(true);
                return;
            }
        }
        var value = e.target.value;
        if (id == "date") {
            value = new Date(value).getTime();
        }
        setData({ ...data, [id]: value });
        setChangeData(true);
    };
    const save = async () => {
        const dataSend = {
            name: data.name,
            last_name: data.last_name,
            card: data.card,
            date: data.date,
            pais: data.pais,
            provincia: data.provincia,
            ciudad: data.ciudad,
            calle: data.calle,
            phone: data.phone,
        };

        const result = await accounts.put(token, dataSend);
        if (result.type === "ok") {
            log("SAVE DATA", dataSend, "green");
            Cookies.set("token", result.token);
            const msjOK = (
                <div className="result error mb-34 color-seafoam-green font-size-25">
                    {result.msj}
                </div>
            );
            setRespond(msjOK);

            setEdit(false);
            setChangeData(false);
        } else {
            const error = (
                <div className="result error mb-34 color-light-salmon font-size-25">
                    {result.msj}
                </div>
            );
            setRespond(error);
        }
    };
    const date = data.date == null ? "" : toStringTwoDigitDate(data.date);
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75">
                <div className="col-12">
                    <Space space="110" />
                </div>
                <div className="col-12 col-sm-10">
                    <h1>Mi Cuenta</h1>
                </div>
                <div className="col-12 col-sm-2 align-right">
                    {edit ? (
                        <a
                            className="color-light-salmon font-size-32"
                            onClick={() => {
                                setData(dataCancel);
                                setEdit(!edit);
                                setChangeData(false);
                            }}
                        >
                            Cancelar
                        </a>
                    ) : (
                        <a
                            className="color-brownish-grey font-size-32"
                            onClick={() => {
                                setDataCancel(data);
                                setEdit(!edit);
                            }}
                        >
                            Editar
                        </a>
                    )}
                </div>
                <div className="col-12">
                    <Space space="10" />
                </div>
                <div className="col-12 col-md-6">
                    <Space space="27" />
                    <Breadcrumbs items={ItemsBreadcrumbs} />
                    <Space space="47" />
                    <p className="mb-28 mt-0">
                        Gestiona los datos básicos de tu cuenta, para que
                        nuestro equipo de servicio al cliente pueda brindarte el
                        mejor soporte.
                    </p>
                </div>
                <div className="col-12 col-md-6">
                    <Input
                        placeholder="Nombre"
                        onChange={updateData("name")}
                        value={data.name}
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                    <Input
                        placeholder="Apellido"
                        onChange={updateData("last_name")}
                        value={data.last_name}
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                </div>
                <div className="col-12 col-md-6">
                    <Select
                        placeholder="Pais"
                        onChange={updateData("pais")}
                        value={data.pais}
                        className="mb-28 w-100"
                        classNameInput="w-100"
                        disabled={!edit}
                        options={paices}
                    />
                    {data.pais === "COL" ? (
                        <Select
                            placeholder="Provincia"
                            onChange={updateData("provincia")}
                            value={data.provincia}
                            className="mb-28 w-100"
                            classNameInput="w-100"
                            disabled={!edit}
                            options={provincias}
                        />
                    ) : (
                        <Input
                            placeholder="Provincia"
                            onChange={updateData("provincia")}
                            value={data.provincia}
                            className="mb-28 w-100"
                            disabled={!edit}
                        />
                    )}
                    {data.pais === "COL" ? (
                        <Select
                            placeholder="Ciudad"
                            onChange={updateData("ciudad")}
                            value={data.ciudad}
                            className="mb-28 w-100"
                            classNameInput="w-100"
                            disabled={!edit}
                            options={ciudades[data.provincia] || {}}
                        />
                    ) : (
                        <Input
                            placeholder="Ciudad"
                            onChange={updateData("ciudad")}
                            value={data.ciudad}
                            className="mb-28 w-100"
                            disabled={!edit}
                        />
                    )}
                    <Input
                        placeholder="Calle"
                        onChange={updateData("calle")}
                        value={data.calle}
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                </div>
                <div className="col-12 col-md-6">
                    <Input
                        placeholder="Card"
                        onChange={updateData("card")}
                        value={data.card}
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                    <Input
                        placeholder="Fecha"
                        onChange={updateData("date")}
                        value={date}
                        type="date"
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                    <Input
                        placeholder="Email"
                        onChange={updateData("email")}
                        value={data.email}
                        type="email"
                        className="mb-28 w-100"
                        disabled={true}
                    />
                    <Input
                        placeholder="Phone"
                        onChange={updateData("phone")}
                        value={data.phone}
                        type="tel"
                        className="mb-28 w-100"
                        disabled={!edit}
                    />
                    {respond}
                    {edit && (
                        <>
                            <Button
                                text="Guardar"
                                className="w-100"
                                onClick={save}
                                disabled={!changeData}
                            />
                            <Space space="30" />
                        </>
                    )}
                </div>
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.account = {
        name: "",
        last_name: "",
        card: "",
        date: null,
        pais: "",
        provincia: "",
        ciudad: "",
        calle: "",
        email: "",
        phone: "",
    };
    if (props.token) {
        const token = props.token;
        props.account = {
            name: "",
            last_name: "",
            card: "",
            date: null,
            pais: "",
            provincia: "",
            ciudad: "",
            calle: "",
            email: "",
            phone: "",
            ...jwt_decode(token),
        };
    }

    return config;
};
export default Index;
