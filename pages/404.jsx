import React, { useState, useEffect } from "react";
import Link from "next/link";


const Index = (props) => {
    return (
        <div className="bg-gradient">
            <div className="container">
                <div className="col-12 align-center min-vh-100 flex flex-align-center">
                    <div>
                        <div className="color-white font-size-50">Upps!!!!</div>
                        <div className="c404">404</div>
                        <Link href="/">
                            <a className="btn">Go to home</a>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Index;
