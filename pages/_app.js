import Head from "next/head";

import "@/styles/globals.css";

function MyApp({ Component, pageProps }) {
    return (
        <>
            <Head>
              <link rel="icon" href="/favicon.svg" type="image/x-icon" />
              <title>TakeProfit</title>
            </Head>
            <Component {...pageProps} />
        </>
    );
}

export default MyApp;
