import React, { useState, useEffect } from "react";
import Link from "next/link";

import getToken from "@/servers/getToken";

import { methods } from "@/api/api";
import { log } from "@/functions/_index";
import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Input from "@/components/input";
import Select from "@/components/select";
import Button from "@/components/button";

const ItemsBreadcrumbs = [
    {
        href: "/retiros/",
        text: "Retiros",
    },
    {
        href: "/retiros/metodos",
        text: "Metodos de Retiro",
    },
];
const Index = ({ token, metodos }) => {
    log(`METHODS`, metodos, "aqua");
    const [respond, setRespond] = useState("");
    const [changeData, setChangeData] = useState(false);
    const [retiros, setRetiros] = useState(metodos);
    const [addRetiro, setAddRetiro] = useState(false);
    const [data, setData] = useState({
        _id: null,
        name: "",
        bank: null,
        account: "",
        typeAccount: null,
    });
    const updateData = (id) => (e) => {
        var value = e.target == null ? e : e.target.value;
        log(`CHANGE ${id}`, value, "aqua");
        setData({ ...data, [id]: value });
        setChangeData(true);
    };
    const save = async () => {
        const sendData = {
            name: data.name,
            bank: data.bank,
            account: data.account,
            typeAccount: data.typeAccount,
        };
        log("SAVE DATA", sendData, "green");
        var swError = false;
        if (data._id === null) {
            const result = await methods.post(token, sendData);
            if (result.type === "ok") {
                const newMethod = result.respond.ops[0];
                setRetiros([...retiros, newMethod]);
            } else {
                swError = true;
                const error = (
                    <div className="result error mb-34 color-light-salmon font-size-25">
                        {result.msj}
                    </div>
                );
                setRespond(error);
            }
        } else {
            const result = await methods.put(token, data._id, sendData);
            if (result.type === "ok") {
                const newRetiros = retiros.map((e) => {
                    if (e._id === data._id) {
                        e = {
                            ...e,
                            ...sendData,
                        };
                    }
                    return e;
                });
                setRetiros(newRetiros);
            } else {
                swError = true;
                const error = (
                    <div className="result error mb-34 color-light-salmon font-size-25">
                        {result.msj}
                    </div>
                );
                setRespond(error);
            }
        }
        if (!swError) {
            setAddRetiro(false);
            setChangeData(false);
        }
    };
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Metodos de Retiro</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 col-md-5">
                    <Breadcrumbs items={ItemsBreadcrumbs} />
                    <Space space="48" />
                    <p className="mb-28 mt-0">
                        Gestiona tus cuentas bancarias para retirar tus fondos,
                        puedes tener varias cuentas bancarias, pero debes tener
                        presente que cada cambio aplicara para el siguiente
                        retiro disponible.
                        <br />
                        <br />
                        Verifica que tu banco se encuentre en la lista de bancos
                        Triple AAA, habilitados por nuestro sistema.
                    </p>
                </div>
                <div className="col-12 col-md-7">
                    {addRetiro ? (
                        <>
                            <div className="align-right mb-48">
                                <a
                                    className="color-light-salmon font-size-32"
                                    onClick={() => {
                                        setAddRetiro(false);
                                        setChangeData(false);
                                    }}
                                >
                                    Cancelar
                                </a>
                            </div>
                            <Input
                                placeholder="Nombre la Cuenta"
                                onChange={updateData("name")}
                                value={data.name}
                                className="mb-26 w-100"
                            />
                            <Select
                                placeholder="Seleccionar Banco"
                                onChange={updateData("bank")}
                                value={data.bank}
                                className="mb-26 w-100"
                                classNameInput="w-100"
                                options={banks}
                            />
                            <Input
                                placeholder="Numero de la cuenta"
                                onChange={updateData("account")}
                                value={data.account}
                                className="mb-26 w-100"
                            />
                            <Select
                                placeholder="Tipo de Cuenta"
                                onChange={updateData("typeAccount")}
                                value={data.typeAccount}
                                className="mb-26 w-100"
                                classNameInput="w-100"
                                options={typeCuentas}
                            />
                            {respond}
                            <Button
                                text="Guardar"
                                className="w-100"
                                disabled={!changeData}
                                onClick={save}
                            />
                        </>
                    ) : (
                        <>
                            {retiros.length === 0 ? (
                                <>
                                    <h4
                                        style={{ lineHeight: 1 }}
                                        className="align-center"
                                    >
                                        No Hay Método de Retiro!
                                    </h4>
                                    <p className="mt-25 mb-25 align-center">
                                        Aun no haz registrado tu primera cuenta
                                        bancaria de retiro.
                                    </p>
                                    <div className="align-center">
                                        <img
                                            src="/img/no-hay-metodo-retiro.svg"
                                            alt="no hay metodo de retiro"
                                        />
                                    </div>
                                </>
                            ) : (
                                <>
                                    <div className="align-center">
                                        {retiros.map((e, i) => {
                                            return (
                                                <div key={i}>
                                                    <div
                                                        className="card mb-20  flex-align-center min-wp-800"
                                                        style={{
                                                            display:
                                                                "inline-flex",
                                                        }}
                                                    >
                                                        <img
                                                            src={`/banks/${e.bank}.png`}
                                                            alt={e.bank}
                                                            className="mr-10"
                                                        />
                                                        <div
                                                            className="info mr-auto align-left"
                                                            style={{
                                                                marginRight:
                                                                    "auto",
                                                            }}
                                                        >
                                                            <strong className="name color-black-two font-size-30">
                                                                {e.name}
                                                            </strong>
                                                            <div className="number color-brownish-grey font-size-30">
                                                                {e.account}
                                                            </div>
                                                            <div className="type color-greyish-two font-size-20">
                                                                {
                                                                    typeCuentas[
                                                                        e
                                                                            .typeAccount
                                                                    ]
                                                                }
                                                            </div>
                                                        </div>
                                                        <a
                                                            className="edit color-brownish-grey font-size-26 noDecoration"
                                                            onClick={() => {
                                                                setData(e);
                                                                setAddRetiro(
                                                                    true
                                                                );
                                                            }}
                                                        >
                                                            Editar
                                                        </a>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </>
                            )}
                            <div className="align-center">
                                <a
                                    className="flex flex-align-center flex-justify-center flex-no-wrap"
                                    onClick={() => {
                                        setData({
                                            _id: null,
                                            name: "",
                                            bank: null,
                                            account: "",
                                            typeAccount: null,
                                        });
                                        setAddRetiro(true);
                                    }}
                                >
                                    <img
                                        src="/img/plus.svg"
                                        alt="plus"
                                        className="mr-28"
                                    />
                                    Agregar cuenta bancaria de retiro
                                </a>
                            </div>
                        </>
                    )}
                </div>
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.metodos = [];
    if (props.token) {
        const token = props.token;
        const result = await methods.get(token);
        if (result.type === "ok") {
            props.metodos = result.respond;
        }
        log("METHODS", props.metodos, "aqua");
    }

    return config;
};
export default Index;
