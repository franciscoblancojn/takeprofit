import React, { useState, useEffect } from "react";
import Link from "next/link";
import exportFromJSON from "export-from-json";

import getToken from "@/servers/getToken";

import { retreats, methods } from "@/api/api";
import { money, log } from "@/functions/_index";
import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Table from "@/components/table";

const ItemsBreadcrumbs = [
    {
        href: "/retiros/",
        text: "Retiros",
    },
    {
        href: "/retiros/historial",
        text: "Historial de Retiros",
    },
];
const Index = ({ token, retreats , metodos}) => {
    log("RETREATS", retreats, "aqua");
    log("methods", metodos, "aqua");
    const exportTable = () => {
        const fileName = new Date().toISOString().split(" ").join("_");
        exportFromJSON({ data: retreats, fileName, exportType: "xls" });
    };
    const metodosSelect = {};
    metodos.forEach((element) => {
        metodosSelect[element._id] = {
            text: element.account,
            option: (
                <div className="flex flex-no-wrap flex-align-center">
                    <img
                        src={`/banks/${element.bank}.png`}
                        alt={element.bank}
                        className="mr-5"
                    />
                    {element.name}
                </div>
            ),
        };
    });
    var periodo = retreats.length 
    const TABLEHEAD = [
        {
            id: "date",
            html: "Fecha",
            print: (e) => {
                return new Date(e).toLocaleDateString();
            },
        },
        {
            id: "methods",
            html: "Metodo",
            print: (e) => {
                return metodosSelect[e].option;
            },
        },
        {
            id: "periodo",
            html: "Periodo",
            print: (e) => {
                return `Periodo ${periodo--}`;
            },
        },
        {
            id: "monto",
            html: "Monto",
            print: (e) => {
                return money(e);
            },
        },
    ];
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Historial de Retiros</h1>
                    <Space space="28" />
                </div>
                <div className="col-12 mb-48">
                    <div className=" flex flex-align-center flex-justify-between w-100">
                        <Breadcrumbs items={ItemsBreadcrumbs} />
                        {retreats.length !== 0 && (
                            <a
                                className="font-size-35 color-warm-grey-two flex flex-align-center flex-no-wrap"
                                onClick={exportTable}
                            >
                                <img
                                    src="/img/export.svg"
                                    alt="export"
                                    className="mr-5"
                                />
                                Exportar
                            </a>
                        )}
                    </div>
                </div>
                {retreats.length === 0 ? (
                    <>
                        <div className="col-12 col-md-5">
                            <p className="mb-28 mt-0">
                                Mira el historial de retiros, realizados y
                                liquidado mensualmente, usualmente cancelados un
                                mes vencido +3 días hábiles.
                                <br />
                                <br />
                                Tiempo el cual tarda el Broker en trasladar los
                                fondos.
                            </p>
                        </div>
                        <div className="col-12 col-md-7">
                            <div className="align-center">
                                <h4 style={{ lineHeight: 1 }}>
                                    No Hay Historial de Retiros!
                                </h4>
                                <p className="mt-26 mb-47 max-wp-487 ml-auto mr-auto">
                                    No registramos historial de retiros hechos
                                    aun en la plataforma.
                                </p>

                                <img src="/img/Artwork.svg" alt="Artwork.svg" />
                            </div>
                        </div>
                    </>
                ) : (
                    <div
                        className="col-12"
                        style={{ maxWidth: "calc(100vw - 30px)" }}
                    >
                        <Table
                            rows={retreats}
                            ths={TABLEHEAD}
                            className="w-100"
                        />
                    </div>
                )}
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.retreats = [];
    props.metodos = [];
    if (props.token) {
        const token = props.token;
        const result = await retreats.get(token);
        if (result.type === "ok") {
            props.retreats = result.respond;
        }
        log("RETREATS", props.retreats, "aqua");
        const resultM = await methods.get(token);
        if (resultM.type === "ok") {
            props.metodos = resultM.respond;
        }
        log("METHODS", props.metodos, "aqua");
    }

    return config;
};
export default Index;
