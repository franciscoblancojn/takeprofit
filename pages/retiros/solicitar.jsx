import React, { useState, useEffect } from "react";
import Link from "next/link";
import jwt_decode from "jwt-decode";

import getToken from "@/servers/getToken";

import { methods, retreats, refreshToken } from "@/api/api";
import { log, money } from "@/functions/_index";
import { banks, typeCuentas } from "@/const/_index";

import Content from "@/components/content";
import Breadcrumbs from "@/components/breadcrumbs";
import Space from "@/components/space";
import Input from "@/components/input";
import Select from "@/components/select";
import Button from "@/components/button";

const ItemsBreadcrumbs = [
    {
        href: "/retiros/",
        text: "Retiros",
    },
    {
        href: "/retiros/gestionar",
        text: "Getionar Retiros",
    },
];
const Index = ({ token, metodos, userToken }) => {
    const [user, setUser] = useState(userToken);
    const maxMonto = (user.capital * user.porRetiro) / 100;
    log("user", user, "aqua");
    log(`METHODS`, metodos, "aqua");
    const [respond, setRespond] = useState("");
    const [changeData, setChangeData] = useState(false);
    const [data, setData] = useState({
        monto: maxMonto,
        methods: "",
    });
    const updateData =
        (id, min = null, max = null) =>
        (e) => {
            var value = e;
            if (e.target) {
                value = e.target.value;
            }
            if (min) {
                value = Math.max(min, value);
            }
            if (max) {
                value = Math.min(max, value);
            }
            const newData = { ...data, [id]: value };
            setData(newData);
        };
    const upDown =
        (id) =>
        (value, min = 1, max = 99999999999) =>
        () => {
            var newValue = parseFloat(data[id]) + value;
            if (!newValue) {
                newValue = min;
            }
            newValue = Math.min(max, Math.max(min, newValue));
            setData({ ...data, [id]: newValue });
        };
    const save = async () => {
        setRespond("");
        const dataSend = {
            monto: parseFloat(data.monto),
            methods: data.methods,
        };
        const result = await retreats.post(token, dataSend);
        if (result.type === "ok") {
            log("SAVE RTREATS", dataSend, "green");
            const msjOK = (
                <div className="result error mb-34 color-seafoam-green font-size-25">
                    Retiro en proceso
                </div>
            );
            const newUser = await refreshToken(token);
            setUser(newUser);
            setRespond(msjOK);
        } else {
            const error = (
                <div className="result error mb-34 color-light-salmon font-size-25">
                    {result.msj}
                </div>
            );
            setRespond(error);
        }
    };
    const metodosSelect = {};
    metodos.forEach((element) => {
        metodosSelect[element._id] = {
            text: element.name,
            option: (
                <div className="flex flex-no-wrap flex-align-center">
                    <img
                        src={`/banks/${element.bank}.png`}
                        alt={element.bank}
                        className="mr-5"
                    />
                    {element.name}
                </div>
            ),
        };
    });
    useEffect(() => {
        if (data.monto != "" && data.methods != "") {
            setChangeData(true);
        }
    }, [data]);
    const now = new Date().getTime();
    const laftRetreatsDate = user.laftRetreatsDate || now;
    const datePermitedStart =
        laftRetreatsDate + user.cooldown * 24 * 60 * 60 * 1000;
    const datePermitedEnd =
        datePermitedStart +
        parseFloat(process.env.DIASPERMITED) * 24 * 60 * 60 * 1000;
    return (
        <Content menu={true}>
            <div className="container pl-xxl-75 pr-xxl-75 pb-20">
                <div className="col-12">
                    <Space space="110" />
                    <h1>Solicitar de Retiro</h1>
                    <Space space="28" />
                    <Breadcrumbs items={ItemsBreadcrumbs} />
                    <Space space="60" />
                    {metodos.length === 0 ? (
                        <>
                            <h4>Debes crear un metodo de retiro primero</h4>
                            <Link href="/retiros/metodos">
                                <a>Ir a Metodos de Retiros</a>
                            </Link>
                        </>
                    ) : (
                        <div className="container">
                            <div className="col-12 col-md-6">
                                <div className="contentInput">
                                    <Input
                                        placeholder={money(data.monto).replace(
                                            "USD",
                                            ""
                                        )}
                                        value={data.monto == "" ? "Monto" : ""}
                                    />
                                    <Input
                                        placeholder="Monto"
                                        value={data.monto}
                                        onChange={updateData(
                                            "monto",
                                            1,
                                            maxMonto
                                        )}
                                        type="number"
                                        moreProps={{ min: 1, max: maxMonto }}
                                        className="overwrite"
                                    />
                                    <div className="arrows  flex flex-align-center flex-column flex-justify-center">
                                        <span
                                            className="up"
                                            onClick={upDown("monto")(
                                                1,
                                                1,
                                                maxMonto
                                            )}
                                        ></span>
                                        <span
                                            className="down"
                                            onClick={upDown("monto")(
                                                -1,
                                                1,
                                                maxMonto
                                            )}
                                        ></span>
                                    </div>
                                </div>
                                <Space space="28" />
                                <Select
                                    placeholder="Metodo"
                                    onChange={updateData("methods")}
                                    value={data.methods}
                                    className="mb-28 "
                                    classNameInput=""
                                    options={metodosSelect || {}}
                                />
                                <Space space="0" />
                                {respond}
                                <Space space="0" />
                                <Button
                                    text="Guardar"
                                    className=""
                                    onClick={save}
                                    disabled={!changeData}
                                />
                            </div>
                            <div className="col-12 col-md-6">
                                <div className="font-size-35">
                                    Tiempo permitido para Retiros{" "}
                                    <div className="flex flex-justify-between">
                                        <strong className="font-size-20">
                                            {new Date(
                                                datePermitedStart
                                            ).toLocaleString()}
                                        </strong>
                                        <strong className="font-size-20">
                                            -
                                        </strong>
                                        <strong className="font-size-20">
                                            {new Date(
                                                datePermitedEnd
                                            ).toLocaleString()}
                                        </strong>
                                    </div>
                                    <br />
                                    Capital{" "}
                                    <strong>{money(user.capital)}</strong>
                                    <br />
                                    <br />
                                    Porcentaje de Retiro{" "}
                                    <strong>{user.porRetiro}%</strong>
                                    <br />
                                    <br />
                                    Monto maximo{" "}
                                    <strong>{money(maxMonto)}</strong>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </Content>
    );
};
export const getServerSideProps = async (ctx) => {
    const config = await getToken(ctx);
    const props = config.props;
    props.metodos = [];
    props.userToken = {};
    if (props.token) {
        const token = props.token;
        const user = jwt_decode(token);
        props.userToken = user;
        const result = await methods.get(token);
        if (result.type === "ok") {
            props.metodos = result.respond;
        }
        log("METHODS", props.metodos, "aqua");
    }

    return config;
};
export default Index;
