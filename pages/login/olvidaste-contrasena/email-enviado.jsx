import Link from "next/link";

import isLogin from "@/servers/isLogin";

import Before from "@/components/before";
import Space from "@/components/space";

const Index = () => {
    return (
        <div className="bg-gradient">
            <div className="container">
                <div className="col-12 min-vh-15">
                    <Space space="50" />
                    <Before />
                    <Space space="50" />
                </div>
                <div className="col-12 align-center min-vh-85 flex flex-space-between">
                    <div>
                        <img src="/img/logoWhite-2.svg" alt="TakeProfit" />
                        <Space space="52" />
                        <h2>Correo Enviado!</h2>
                        <Space space="20" />
                        <img src="/img/planet.svg" alt="planet" />
                        <p className="color-white">
                            Verifica la bandeja de tu correo para
                            <br />
                            proceder a recuperar tu contraseña
                        </p>
                        <Space space="23" />
                        <Link href="/">
                            <a className="btn w-100 d-block">Listo</a>
                        </Link>
                    </div>
                    <div style={{ marginBottom: "auto", width: "100%" }}></div>
                    <div>
                        <Space space="30" />
                        <Link href="#">
                            <a
                                className="color-white font-size-22"
                                style={{ marginRight: "10px" }}
                            >
                                Terminos
                            </a>
                        </Link>
                        <Link href="#">
                            <a className="color-white font-size-22">
                                Politicas de Privacidad
                            </a>
                        </Link>
                        <Space space="19" />
                    </div>
                </div>
            </div>
        </div>
    );
};
export const getServerSideProps = isLogin
export default Index;
