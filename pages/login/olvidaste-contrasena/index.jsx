import React, { useRef,useState } from "react";
import { useRouter } from 'next/router'
import Link from "next/link";

import isLogin from "@/servers/isLogin";

import Input from "@/components/input";
import Before from "@/components/before";
import Space from "@/components/space";
import Button from "@/components/button";

const Index = () => {
    const router = useRouter()
    const [data, setData] = useState({
        email:"",
    })
    const send = () => {
        router.push("/login/olvidaste-contrasena/email-enviado")
    }
    const updateData = (id) => (e) => {
        const value = e.target.value
        setData({...data,[id]:value})
    }
    return (
        <div className="bg-gradient">
            <div className="container">
                <div className="col-12 min-vh-15">
                    <Space space="50" />
                    <Before/>
                    <Space space="50" />
                </div>
                <div className="col-12 align-center min-vh-85 flex flex-space-between">
                    <div>
                        <img src="/img/logoWhite-2.svg" alt="TakeProfit" />
                        <Space space="93" />
                        <h3>Olvidaste tu Contraseña</h3>
                        <Space space="43" />
                        <Input
                            placeholder="Correo Electronico"
                            className="input-2 w-100"
                            onChange={updateData("email")}
                            type="email"
                        />
                        <Space space="23" />
                        <Button text="Enviar" className="w-100" onClick={send}/>
                    </div>
                    <div style={{ marginBottom: "auto", width: "100%" }}></div>
                    <div>
                        <Space space="30" />
                        <Link href="#">
                            <a
                                className="color-white font-size-22"
                                style={{ marginRight: "10px" }}
                            >
                                Terminos
                            </a>
                        </Link>
                        <Link href="#">
                            <a className="color-white font-size-22">
                                Politicas de Privacidad
                            </a>
                        </Link>
                        <Space space="19" />
                    </div>
                </div>
            </div>
        </div>
    );
};
export const getServerSideProps = isLogin
export default Index;
