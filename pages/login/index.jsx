import React, { useRef,useState } from "react";
import Link from "next/link";
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'

import {auth} from "@/api/api";
import isLogin from "@/servers/isLogin";
import { log } from "@/functions/_index";

import Input from "@/components/input";
import InputPassword from "@/components/inputPassword";
import Space from "@/components/space";
import Button from "@/components/button";

const Index = () => {
    const router = useRouter()
    const [respond, setRespond] = useState("")
    const [data, setData] = useState({
        email:"",
        password:""
    })
    const login = async () => {
        const result = await auth(data)
        log(`DATA SEND`, data, "cornflowerblue");
        log(`RESULT`, result, "orange");
        if(result.type=="error"){
            const error = (
                <div className="result error mb-34 color-light-salmon font-size-30">
                    {result.msj}
                </div>
            )
            setRespond(error)
            return;
        }
        setRespond("")
        const token = result.respond.token
        log(`TOKEN`, token, "aqua");
        Cookies.set('token', token)
        router.push("/")
    }
    const updateData = (id) => (e) => {
        const value = e.target.value
        setData({...data,[id]:value})
    }
    return (
        <div className="bg-gradient">
            <div className="container">
                <div className="col-12 align-center min-vh-100 flex flex-space-between">
                    <div>
                        <Space space="100" />
                        <img src="/img/logoWhite-2.svg" alt="TakeProfit" />
                        <Space space="93" />
                        <h2>Entrar</h2>
                        <Space space="43" />
                        <Input
                            placeholder="Correo Electronico"
                            className="input-2 w-100"
                            onChange={updateData("email")}
                        />
                        <Space space="23" />
                        <InputPassword
                            placeholder="Contraseña"
                            className="input-2 w-100"
                            classNameContent="w-100"
                            onChange={updateData("password")}
                        />
                        <Space space="23" />
                        {respond}
                        <Button text="Ingresar" className="w-100" onClick={login}/>
                        <Space space="34" />
                        <Link href="/login/olvidaste-contrasena">
                            <a className="color-white font-size-26">
                                Olvidaste tu contraseña?{" "}
                            </a>
                        </Link>
                    </div>
                    <div style={{ marginBottom: "auto", width: "100%" }}></div>
                    <div>
                        <Space space="30" />
                        <Link href="#">
                            <a
                                className="color-white font-size-22"
                                style={{ marginRight: "10px" }}
                            >
                                Terminos
                            </a>
                        </Link>
                        <Link href="#">
                            <a className="color-white font-size-22">
                                Politicas de Privacidad
                            </a>
                        </Link>
                        <Space space="19" />
                    </div>
                </div>
            </div>
        </div>
    );
};
export const getServerSideProps = isLogin
export default Index;
