import request from "@/api/request";

const verify = async (token) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}auth/verify`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    return result;
};
export default verify;
