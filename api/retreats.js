import request from "@/api/request";

const get = async (token) => {
    const result = await request({
        method: "get",
        url: `${process.env.URLAPI}retreats`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    return result;
}
const post = async (token,data) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}retreats`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data
    });
    return result;
}
export default {
    get,
    post
}