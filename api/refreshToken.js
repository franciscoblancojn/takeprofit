import request from "@/api/request";
import Cookies from 'js-cookie'
import { expireToken } from "@/functions/_index";
import jwt_decode from "jwt-decode";

const refreshToken = async (token) => {
    const result = await request({
        method: "post",
        url: `${process.env.URLAPI}auth/token`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
    });
    if(result.type=="error"){
        expireToken();
        return;
    }
    const newToken = result.respond.token
    Cookies.set('token', newToken)
    const user = jwt_decode(newToken);
    return user;
};
export default refreshToken;
