import request from "@/api/request";

const put = async (token,data) => {
    const result = await request({
        method: "put",
        url: `${process.env.URLAPI}accounts`,
        headers: {
            apikey: process.env.APIKEY,
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
        },
        data: JSON.stringify(data)
    });
    return result;
}
export default {
    put,
}