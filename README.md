# TakeProfit

Page in NextJs for TakeProfit

### Zeplin

[https://app.zeplin.io/project/6193ec30ded466ab8a199f61](https://app.zeplin.io/project/6193ec30ded466ab8a199f61)

### Gitlab 
[https://gitlab.com/franciscoblancojn/takeprofit](https://gitlab.com/franciscoblancojn/takeprofit)

### Vercel

[https://takeprofit.vercel.app/](https://takeprofit.vercel.app/)

### Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)